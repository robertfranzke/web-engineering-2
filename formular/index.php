<!DOCTYPE html>
<html>
<head>

</head>
<body>
<?php
session_start();
if(!$_SESSION['login']){
    header("Location: login.php");
}?>
<h1>Auswahl</h1>
<form>
    <table>
        <tr>
            <td><label>Name</label></td>
            <td><input name="vorn" type="text" maxlength="255" value="<?= $_SESSION['name'] ?>"></td>
        </tr>
        <tr>
            <td><label>Vorname</label></td>
            <td><input name="nachn" type="text" maxlength="255" value="<?= $_SESSION['vname'] ?>"></td>
        </tr>
        <tr>
            <td><label>Email</label></td>
            <td><input name="mail" type="text" maxlength="255" value="<?= $_SESSION['mail'] ?>"></td>
        </tr>
        <tr>
            <td><label>Betreff:</label></td>
            <td><textarea name="betreff" cols="35" rows="7" id="betreff">Ihre Nachricht ... </textarea></td>
        </tr>
        <tr>
            <td><label>Essen:</label></td>
            <td>
                <select name="essen">
                    <option>Wurst und Pommes</option>
                    <option>Pizza</option>
                    <option>Nudeln</option>
                    <option>Burger</option>
                </select>
            </td>
        </tr>
        <tr>
            <td><label>Personenzahl: </label></td>
            <td>
                <input type="radio" id="radio1" name="personenzahl" value="1">
                <label for="radio1">1</label>
                <input type="radio" id="radio2" name="personenzahl" value="2">
                <label for="radio2">2</label>
                <input type="radio" id="radio3" name="personenzahl" value="3">
                <label for="radio3">3</label>
                <input type="radio" id="radio4" name="personenzahl" value="4">
                <label for="radio4">4</label>
                <input type="radio" id="radio5" name="personenzahl" value="5">
                <label for="radio5">5</label>
            </td>
        </tr>
        <tr>
            <td><label>Mitbringsel:</label></td>
            <td>
                    <label>Cola<input type="checkbox" name="bringsel[]" value="cola"></label>
                    <label> Fanta<input type="checkbox" name="bringsel[]" value="fanta"></label>
                    <label> Vodka<input type="checkbox" name="bringsel[]" value="vodka"></label>
                    <label> Chips<input type="checkbox" name="bringsel[]" value="chips"></label>
                    <label> Haribo<input type="checkbox" name="bringsel[]" value="haribo"></label>
            </td>
        </tr>
    </table>
    <button type="submit">senden</button>
    <button type="reset">suchen</button>
    <a href="logout.php"><button type="button">Mach Session kaputt!</button></a>
</form>
<br>
<form enctype="multipart/form-data" action="upload.php" method="post">
<input type="hidden" name="max_size" value="100000">
Ihre Dateiauswahl: <input name="upfile" type="file">
<input type="submit" value="Datei senden">
</form>



<?php
if (isset($_GET["vorn"])) {
    echo "<h1>Ergebnis</h1>";
    echo "<p>Name: " . $_GET['vorn'] . " " . $_GET['nachn'] . "</p>";
    echo "<p>Mail: " . $_GET['mail'] . "</p>";
    echo "<p>Nachricht: " . $_GET['betreff'] . "</p>";
    echo "<p>Essen: " . $_GET['essen'] . "</p>";
}
if (isset($_GET["personenzahl"])) {
    echo "<p>Personenzahl: " . $_GET['personenzahl'] . "</p>";
}
if (isset($_GET["bringsel"])) {
    echo "<p>Mitbringsel: " . implode( ", ", $_GET['bringsel'] ) . "</p>";
}

?>
</body>
</html>